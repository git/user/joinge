# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit eutils

DESCRIPTION="The AndroVM OpenGL Player"
HOMEPAGE="http://http://androvm.org"
SRC_URI="http://androvm.org/Download/AndroVMplayer-Linux64-20130208.tgz"

MERGE_TYPE="binary"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND="
   >=dev-util/androvm-4.1.1-r6
"
RDEPEND="${DEPEND}"

INSTALL_DIR="/opt/${P}"

S="${WORKDIR}/AndroVMplayer-Linux64"


pkg_preinst(){
    dodir "${INSTALL_DIR}"
    cp -rpP ${S}/* "${D}${INSTALL_DIR}" || die
    
    mkdir ${FILESDIR}
    echo "PATH=\"/${INSTALL_DIR}:${INSTALL_DIR}/bin\"" > "${FILESDIR}/99${PN}"
    doenvd "${FILESDIR}/99${PN}"
}


pkg_postinst(){
    env-update
}
