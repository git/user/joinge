# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

DESCRIPTION="The AMD CodeXL Suite"
HOMEPAGE="http://developer.amd.com/tools-and-sdks/heterogeneous-computing/codexl/"
SRC_URI="http://developer.amd.com/download/AMD_CodeXL_Linux_x86_64_${PV}.tar_.gz"

MERGE_TYPE="binary"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64"

DEPEND="
   >=x11-libs/gtkglext-1.0
   =media-libs/libpng-1.2*
   >=sys-devel/gcc-3.0
   >=sys-libs/glibc-2.12
   virtual/libstdc++
   >=media-libs/mesa-7.0
   >=dev-libs/libxml2-2.0
   >=x11-libs/gtk+-2.0
   >=dev-libs/glib-2.0
   >=x11-libs/libXext-1.0
   >=sys-libs/zlib-1.0
   >=x11-libs/libXmu-1.0
   >=x11-libs/libXt-1.0
   >=x11-libs/libSM-1.0
   >=x11-libs/libICE-1.0
   >=x11-libs/pango-1.0
   >=x11-libs/libX11-1.0
   >=dev-libs/atk-1.0
   >=x11-libs/cairo-1.0
   >=media-libs/freetype-2.0
   >=media-libs/fontconfig-2.0
   >=x11-libs/libXfixes-4.0
   >=x11-libs/libXrender-0.9
   >=x11-libs/libXinerama-1.0
   >=x11-libs/libXi-1.0
   >=x11-libs/libXrandr-1.0
   >=x11-libs/libXcursor-1.0
   >=x11-libs/libXcomposite-0.4
   >=x11-libs/libXdamage-1.0
   >=x11-libs/libxcb-1.0
   >=sys-libs/libselinux-2.0
   >=x11-libs/pixman-0.15
   >=dev-libs/expat-2.0
   >=x11-libs/libXau-1.0
"

# LIBGL_DEBUG=verbose LD_LIBRARY_PATH=$(pwd):/usr/lib64/opengl/ati/lib ./CodeXL-bin


# Dependencies listed in the rpm-file (the ones accounted for are double-commented)
#
# #          gcc >= 3.0 is needed by AMD_CodeXL_Linux-1.3.3449-0.x86_64     
# #          glibc >= 2.12 is needed by AMD_CodeXL_Linux-1.3.3449-0.x86_64     
# #          libstdc++ >= 4.0 is needed by AMD_CodeXL_Linux-1.3.3449-0.x86_64     
# #          gtkglext-libs >= 1.0 is needed by AMD_CodeXL_Linux-1.3.3449-0.x86_64     
#          mesa-libGL >= 7.0 is needed by AMD_CodeXL_Linux-1.3.3449-0.x86_64      
#          mesa-libGLU >= 7.0 is needed by AMD_CodeXL_Linux-1.3.3449-0.x86_64      
# #          libxml2 >= 2.0 is needed by AMD_CodeXL_Linux-1.3.3449-0.x86_64     
# #          gtk2 >= 2.0 is needed by AMD_CodeXL_Linux-1.3.3449-0.x86_64     
# #          glib2 >= 2.0 is needed by AMD_CodeXL_Linux-1.3.3449-0.x86_64     
# #          libXext >= 1.0 is needed by AMD_CodeXL_Linux-1.3.3449-0.x86_64     
# #          zlib >= 1.0 is needed by AMD_CodeXL_Linux-1.3.3449-0.x86_64     
# #          libXmu >= 1.0 is needed by AMD_CodeXL_Linux-1.3.3449-0.x86_64     
# #          libXt >= 1.0 is needed by AMD_CodeXL_Linux-1.3.3449-0.x86_64     
# #          libSM >= 1.0 is needed by AMD_CodeXL_Linux-1.3.3449-0.x86_64     
# #          libICE >= 1.0 is needed by AMD_CodeXL_Linux-1.3.3449-0.x86_64     
# #          pango >= 1.0 is needed by AMD_CodeXL_Linux-1.3.3449-0.x86_64     
# #          libX11 >= 1.0 is needed by AMD_CodeXL_Linux-1.3.3449-0.x86_64     
# #          atk >= 1.0 is needed by AMD_CodeXL_Linux-1.3.3449-0.x86_64     
# #          cairo >= 1.0 is needed by AMD_CodeXL_Linux-1.3.3449-0.x86_64     
# #          freetype >= 2.0 is needed by AMD_CodeXL_Linux-1.3.3449-0.x86_64     
# #          fontconfig >= 2.0 is needed by AMD_CodeXL_Linux-1.3.3449-0.x86_64     
# #          libXfixes >= 4.0 is needed by AMD_CodeXL_Linux-1.3.3449-0.x86_64     
# #          libXrender >= 0.9 is needed by AMD_CodeXL_Linux-1.3.3449-0.x86_64     
# #          libXinerama >= 1.0 is needed by AMD_CodeXL_Linux-1.3.3449-0.x86_64     
# #          libXi >= 1.0 is needed by AMD_CodeXL_Linux-1.3.3449-0.x86_64     
# #          libXrandr >= 1.0 is needed by AMD_CodeXL_Linux-1.3.3449-0.x86_64     
# #          libXcursor >= 1.0 is needed by AMD_CodeXL_Linux-1.3.3449-0.x86_64     
# #          libXcomposite >= 0.4 is needed by AMD_CodeXL_Linux-1.3.3449-0.x86_64     
# #          libXdamage >= 1.0 is needed by AMD_CodeXL_Linux-1.3.3449-0.x86_64     
# #          libuuid >= 2.0 is needed by AMD_CodeXL_Linux-1.3.3449-0.x86_64      
# #          libxcb >= 1.0 is needed by AMD_CodeXL_Linux-1.3.3449-0.x86_64     
# #          libselinux >= 2.0 is needed by AMD_CodeXL_Linux-1.3.3449-0.x86_64      
# #          libpng >= 1.0 is needed by AMD_CodeXL_Linux-1.3.3449-0.x86_64     
# #          pixman >= 0.15 is needed by AMD_CodeXL_Linux-1.3.3449-0.x86_64     
# #          expat >= 2.0 is needed by AMD_CodeXL_Linux-1.3.3449-0.x86_64     
# #          libXau >= 1.0 is needed by AMD_CodeXL_Linux-1.3.3449-0.x86_64     
# #          /bin/sh is needed by AMD_CodeXL_Linux-1.3.3449-0.x86_64     


RDEPEND="${DEPEND}"

S="${WORKDIR}/AMD_CodeXL_Linux_x86_64_${PV}/Output_x86_64/release"

CODEXL_DIR="opt/${P}"
# pkg_setup() {
# # 	enewgroup android
# }
# 
pkg_preinst(){
    dodir "${CODEXL_DIR}"
    cp -rpP ${S}/* "${D}${CODEXL_DIR}" || die
    
    mkdir ${FILESDIR}
    echo "PATH=\"/${CODEXL_DIR}/bin\"" > "${FILESDIR}/99${PN}"
    echo "ROOTPATH=\"/${CODEXL_DIR}/bin\"" > "${FILESDIR}/99${PN}"
    doenvd "${FILESDIR}/99${PN}"
#     mv AMD_CodeXL_Linux_x86_64_${PV}/Output_x86_64/release amdapp-codecl
# 	rm -rf tools/lib/x86*
}

pkg_postinst(){
    env-update
}
# 
# # src_configure() {
# #     econf --with-posix-regex
# # }
# 
# src_install(){
# 	declare CODEXL_DIR="/opt/${PN}"
# 	
# 	mv "${S}" "${D}"${MOZILLA_FIVE_HOME} || die
# 
# }

# src_install(){
# # 	dodoc tools/NOTICE.txt "SDK Readme.txt" || die
# # 	rm -f tools/NOTICE.txt "SDK Readme.txt"
# 
# 	dodir "${CODEXL_DIR}"
# 	cp -pPR AMD_CodeXL_Linux_x86_64_1.2.2484/Output_x86_64/release/* "${CODEXL_DIR}" || die "failed to install tools"
# 
# 	# Maybe this is needed for the tools directory too.
# # 	dodir "${ANDROID_SDK_DIR}"/{add-ons,build-tools,docs,extras,platforms,platform-tools,samples,sources,system-images,temp} || die "failed to dodir"
# 
# # 	fowners root:android "${ANDROID_SDK_DIR}"/{.,add-ons,build-tools,docs,extras,platforms,platform-tools,samples,sources,system-images,temp,tools} || die
# # 	fperms 0775 "${ANDROID_SDK_DIR}"/{.,add-ons,build-tools,docs,extras,platforms,platform-tools,samples,sources,system-images,temp,tools} || die
# # 
# # 	echo "PATH=\"${EPREFIX}${ANDROID_SDK_DIR}/tools:${EPREFIX}${ANDROID_SDK_DIR}/platform-tools\"" > "${T}/80${PN}" || die
# # 
# # 	SWT_PATH=
# # 	SWT_VERSIONS="3.7 3.6"
# # 	for version in $SWT_VERSIONS; do
# # 		# redirecting stderr to /dev/null
# # 		# not sure if this is best, but avoids misleading error messages
# # 		SWT_PATH="`dirname \`java-config -p swt-\$version 2>/dev/null\` 2>/dev/null`"
# # 		if [ $SWT_PATH ]; then
# # 			einfo "SWT_PATH=$SWT_PATH selecting version $version of SWT."
# # 			break
# # 		fi
# # 	done
# # 
# # 	echo "ANDROID_SWT=\"${SWT_PATH}\"" >> "${T}/80${PN}" || die
# # 
# # 	doenvd "${T}/80${PN}" || die
# # 
# # 	echo "SEARCH_DIRS_MASK=\"${EPREFIX}${ANDROID_SDK_DIR}\"" > "${T}/80${PN}" || die
# # 
# # 	insinto "/etc/revdep-rebuild" && doins "${T}/80${PN}" || die
# 
# }

# src_install() {
#     emake DESTDIR="${D}" install || die
# 
#     dodoc FAQ NEWS README
#     dohtml EXTENDING.html ctags.html
# 
# }
#src_install() {
	# You must *personally verify* that this trick doesn't install
	# anything outside of DESTDIR; do this by reading and
	# understanding the install part of the Makefiles.
	# This is the preferred way to install.
	#emake DESTDIR="${D}" install || die

	# When you hit a failure with emake, do not just use make. It is
	# better to fix the Makefiles to allow proper parallelization.
	# If you fail with that, use "emake -j1", it's still better than make.

	# For Makefiles that don't make proper use of DESTDIR, setting
	# prefix is often an alternative.  However if you do this, then
	# you also need to specify mandir and infodir, since they were
	# passed to ./configure as absolute paths (overriding the prefix
	# setting).
	#emake \
	#	prefix="${D}"/usr \
	#	mandir="${D}"/usr/share/man \
	#	infodir="${D}"/usr/share/info \
	#	libdir="${D}"/usr/$(get_libdir) \
	#	install || die
	# Again, verify the Makefiles!  We don't want anything falling
	# outside of ${D}.

	# The portage shortcut to the above command is simply:
	#
	#einstall || die
#}
