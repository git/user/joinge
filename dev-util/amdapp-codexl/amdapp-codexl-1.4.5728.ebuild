# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

DESCRIPTION="The AMD CodeXL Suite"
HOMEPAGE="http://developer.amd.com/tools-and-sdks/heterogeneous-computing/codexl/"
SRC_URI="http://developer.amd.com/download/AMD_CodeXL_Linux_x86_64_${PV}.tar.gz"

MERGE_TYPE="binary"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64"

DEPEND="
   >=x11-libs/gtkglext-1.0
   =media-libs/libpng-1.2*
   >=sys-devel/gcc-3.0
   >=sys-libs/glibc-2.12
   virtual/libstdc++
   >=media-libs/mesa-7.0
   >=dev-libs/libxml2-2.0
   >=x11-libs/gtk+-2.0
   >=dev-libs/glib-2.0
   >=x11-libs/libXext-1.0
   >=sys-libs/zlib-1.0
   >=x11-libs/libXmu-1.0
   >=x11-libs/libXt-1.0
   >=x11-libs/libSM-1.0
   >=x11-libs/libICE-1.0
   >=x11-libs/pango-1.0
   >=x11-libs/libX11-1.0
   >=dev-libs/atk-1.0
   >=x11-libs/cairo-1.0
   >=media-libs/freetype-2.0
   >=media-libs/fontconfig-2.0
   >=x11-libs/libXfixes-4.0
   >=x11-libs/libXrender-0.9
   >=x11-libs/libXinerama-1.0
   >=x11-libs/libXi-1.0
   >=x11-libs/libXrandr-1.0
   >=x11-libs/libXcursor-1.0
   >=x11-libs/libXcomposite-0.4
   >=x11-libs/libXdamage-1.0
   >=x11-libs/libxcb-1.0
   >=sys-libs/libselinux-2.0
   >=x11-libs/pixman-0.15
   >=dev-libs/expat-2.0
   >=x11-libs/libXau-1.0
"

# LIBGL_DEBUG=verbose LD_LIBRARY_PATH=$(pwd):/usr/lib64/opengl/ati/lib ./CodeXL-bin


# Dependencies listed in the rpm-file (the ones accounted for are double-commented)
#
# #        gcc >= 3.0 is needed by AMD_CodeXL_Linux-1.4.5728-0.x86_64
# #        glibc >= 2.12 is needed by AMD_CodeXL_Linux-1.4.5728-0.x86_64
# #        libstdc++ >= 4.0 is needed by AMD_CodeXL_Linux-1.4.5728-0.x86_64
# #        gtkglext-libs >= 1.0 is needed by AMD_CodeXL_Linux-1.4.5728-0.x86_64
#         mesa-libGL >= 7.0 is needed by AMD_CodeXL_Linux-1.4.5728-0.x86_64
#         mesa-libGLU >= 7.0 is needed by AMD_CodeXL_Linux-1.4.5728-0.x86_64
# #        libxml2 >= 2.0 is needed by AMD_CodeXL_Linux-1.4.5728-0.x86_64
# #        gtk2 >= 2.0 is needed by AMD_CodeXL_Linux-1.4.5728-0.x86_64
# #        glib2 >= 2.0 is needed by AMD_CodeXL_Linux-1.4.5728-0.x86_64
# #        libXext >= 1.0 is needed by AMD_CodeXL_Linux-1.4.5728-0.x86_64
# #        zlib >= 1.0 is needed by AMD_CodeXL_Linux-1.4.5728-0.x86_64
# #        libXmu >= 1.0 is needed by AMD_CodeXL_Linux-1.4.5728-0.x86_64
# #        libXt >= 1.0 is needed by AMD_CodeXL_Linux-1.4.5728-0.x86_64
# #        libSM >= 1.0 is needed by AMD_CodeXL_Linux-1.4.5728-0.x86_64
# #        libICE >= 1.0 is needed by AMD_CodeXL_Linux-1.4.5728-0.x86_64
# #        pango >= 1.0 is needed by AMD_CodeXL_Linux-1.4.5728-0.x86_64
# #        libX11 >= 1.0 is needed by AMD_CodeXL_Linux-1.4.5728-0.x86_64
# #        atk >= 1.0 is needed by AMD_CodeXL_Linux-1.4.5728-0.x86_64
# #        cairo >= 1.0 is needed by AMD_CodeXL_Linux-1.4.5728-0.x86_64
# #        freetype >= 2.0 is needed by AMD_CodeXL_Linux-1.4.5728-0.x86_64
# #        fontconfig >= 2.0 is needed by AMD_CodeXL_Linux-1.4.5728-0.x86_64
# #        libXfixes >= 4.0 is needed by AMD_CodeXL_Linux-1.4.5728-0.x86_64
# #        libXrender >= 0.9 is needed by AMD_CodeXL_Linux-1.4.5728-0.x86_64
# #        libXinerama >= 1.0 is needed by AMD_CodeXL_Linux-1.4.5728-0.x86_64
# #        libXi >= 1.0 is needed by AMD_CodeXL_Linux-1.4.5728-0.x86_64
# #        libXrandr >= 1.0 is needed by AMD_CodeXL_Linux-1.4.5728-0.x86_64
# #        libXcursor >= 1.0 is needed by AMD_CodeXL_Linux-1.4.5728-0.x86_64
# #        libXcomposite >= 0.4 is needed by AMD_CodeXL_Linux-1.4.5728-0.x86_64
# #        libXdamage >= 1.0 is needed by AMD_CodeXL_Linux-1.4.5728-0.x86_64
# #        libuuid >= 2.0 is needed by AMD_CodeXL_Linux-1.4.5728-0.x86_64
# #        libxcb >= 1.0 is needed by AMD_CodeXL_Linux-1.4.5728-0.x86_64
# #        libselinux >= 2.0 is needed by AMD_CodeXL_Linux-1.4.5728-0.x86_64
# #        libpng >= 1.0 is needed by AMD_CodeXL_Linux-1.4.5728-0.x86_64
# #        pixman >= 0.15 is needed by AMD_CodeXL_Linux-1.4.5728-0.x86_64
# #        expat >= 2.0 is needed by AMD_CodeXL_Linux-1.4.5728-0.x86_64
# #        libXau >= 1.0 is needed by AMD_CodeXL_Linux-1.4.5728-0.x86_64
# #        /bin/sh is needed by AMD_CodeXL_Linux-1.4.5728-0.x86_64

RDEPEND="${DEPEND}"

S="${WORKDIR}/AMD_CodeXL_Linux_x86_64_${PV}"

CODEXL_DIR="opt/${P}"

pkg_preinst(){
    dodir "${CODEXL_DIR}"
    cp -rpP ${S}/* "${D}${CODEXL_DIR}" || die
    
    mkdir ${FILESDIR}
    echo "PATH=\"/${CODEXL_DIR}\"" > "${FILESDIR}/99${PN}"
    echo "ROOTPATH=\"/${CODEXL_DIR}\"" >> "${FILESDIR}/99${PN}"
    doenvd "${FILESDIR}/99${PN}"
#     mv AMD_CodeXL_Linux_x86_64_${PV}/Output_x86_64/release amdapp-codecl
# 	rm -rf tools/lib/x86*
}

pkg_postinst(){
    env-update
    
   elog "AMD CodeXL is now fetched and installed to /${CODEXL_DIR}"
   elog ""
   elog "For some reason it seems necessary to preload the OpenGL library:"
   elog ">> LD_LIBRARY_PATH=/usr/lib64/opengl/ati/lib CodeXL"
   elog ""
}

