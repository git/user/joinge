# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit eutils

DESCRIPTION="The AndroVM System"
HOMEPAGE="http://http://androvm.org"
# EGIT_REPO_URI=""
# SRC_URI="http://developer.amd.com/download/AMD_CodeXL_Linux_x86_64_${PV}.tar.gz"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64 ~x86"

# FEATURES="-sandbox"

PYTHON_COMPAT=( python{2_6,2_7} )

#   <sys-devel/make-3.82

DEPEND="
   >=dev-vcs/git-1.7.2
   sys-devel/make
   =dev-java/sun-jdk-1.6*
   >=app-crypt/gnupg-2.0.20
   >=sys-devel/flex-2.5.37 
   >=sys-devel/bison-2.4.3 
   >=dev-util/gperf-3.0.4
   >=media-libs/libsdl-1.2.15-r2
   >=x11-libs/wxGTK-2.8.12.1:2.8
   >=sys-fs/squashfs-tools-4.2
   >=net-misc/curl-7.31.0
   =sys-libs/ncurses-5.9-r2:5
   >=sys-libs/zlib-1.2.7
   >=sys-process/schedtool-1.3.0-r1
   >=virtual/perl-Switch-2.160.0
   >=app-arch/zip-3.0-r1
   >=app-arch/unzip-6.0-r3
   >=media-sound/pulseaudio-2.1-r1
   >=media-libs/alsa-lib-1.0.27.1
   >=dev-util/valgrind-3.8.1
   >=app-emulation/emul-linux-x86-baselibs-20130224
   >=app-emulation/emul-linux-x86-compat-20130224
   >=app-emulation/emul-linux-x86-cpplibs-20130224
   || ( =dev-lang/python-2.6* =dev-lang/python-2.7* )
   dev-util/repo
"

RDEPEND="${DEPEND}"

INSTALL_DIR="/opt/${P}"

##
## NOTE
##
## gpg --import is not handled (yet)
##

pkg_setup() {
   mkdir -p "${S}"
   cd "${S}"

   if [ -d "${INSTALL_DIR}/.repo" ]; then
      rsync -arvu "${INSTALL_DIR}/.repo/" "${S}/.repo"
   fi
   chown portage:portage -R "${S}"

}

src_unpack() {
   return
}

src_prepare(){

   repo init -u http://github.com/androvm/platform_manifest.git -b genymotion-4.2.2_r1.2 || die
   repo sync || die
#    make update-api

}

src_configure(){
#    source build/envsetsup.sh || die
   return
}

src_compile(){
   return
#    make ARCH=x86 -j12 || die
#    make ARCH=x86 android_disk_vdi -j12 || die
}

src_install(){

#    dodir "${INSTALL_DIR}"
#    cp -rpP "${S}/" "${D}${INSTALL_DIR}/" || die
#    
   exeinto "${INSTALL_DIR}"  
   doexe "${FILESDIR}/mysetup.sh"
   doexe "${FILESDIR}/mybuild.sh"
   doexe "${FILESDIR}/myinstall.sh"

}

pkg_preinst(){

    enewgroup androvm
    enewuser androvm -1 -1 -1 androvm
}

pkg_postinst(){

   rsync -arvu "${S}/" "${INSTALL_DIR}"
   
   chown androvm:androvm -R "${INSTALL_DIR}"
   chmod g+w -R "${INSTALL_DIR}"
   
   elog "Importing gpg key..."
   gpg --import "${FILESDIR}/gpg.key"
   
   elog "AndroVM is now fetched and installed to ${INSTALL_DIR}"
   elog ""
   elog "Next steps:"
   elog ">> cd ${INSTALL_DIR}"
   elog ">> source ./mysetup.sh"
   elog ">> ./mybuild.sh"
   elog ""
   ewarn "Make sure you are in the androvm group to avoid permission issues:"
   ewarn ">> gpasswd -a <user> androvm"
   
}
