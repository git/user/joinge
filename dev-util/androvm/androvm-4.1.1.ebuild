# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit eutils

DESCRIPTION="The AndroVM System"
HOMEPAGE="http://http://androvm.org"
# EGIT_REPO_URI=""
# SRC_URI="http://developer.amd.com/download/AMD_CodeXL_Linux_x86_64_${PV}.tar.gz"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64 ~x86"

# FEATURES="-sandbox"s

PYTHON_COMPAT=( python{2_6,2_7} )
DEPEND="
   >=dev-vcs/git-1.7
   <sys-devel/make-3.82
   =dev-java/sun-jdk-1.6*
   >=app-crypt/gnupg-2.0.20
   >=sys-devel/flex-2.5.37 
   >=sys-devel/bison-2.4.3 
   >=dev-util/gperf-3.0.4
   >=media-libs/libsdl-1.2.15-r2
   >=x11-libs/wxGTK-2.8.12.1:2.8
   >=sys-fs/squashfs-tools-4.2
   >=net-misc/curl-7.31.0
   =sys-libs/ncurses-5.9-r2:5
   >=sys-libs/zlib-1.2.7
   >=sys-process/schedtool-1.3.0-r1
   >=virtual/perl-Switch-2.160.0
   >=app-arch/zip-3.0-r1
   >=app-arch/unzip-6.0-r3
   >=media-sound/pulseaudio-2.1-r1
   >=media-libs/alsa-lib-1.0.27.1
   >=dev-util/valgrind-3.8.1
   >=app-emulation/emul-linux-x86-baselibs-20130224
   >=app-emulation/emul-linux-x86-compat-20130224
   >=app-emulation/emul-linux-x86-cpplibs-20130224
   >=dev-lang/python-2.7.5-r2:2.7
   >=dev-util/repo-1.19
"

RDEPEND="${DEPEND}"

INSTALL_DIR="/opt/${P}"

##
## NOTE
##
## gpg --import is not handled (yet)
##

pkg_setup() {
   mkdir -p "${S}"

   if [ -d "${INSTALL_DIR}/.repo" ]; then
      rsync -arvu "${INSTALL_DIR}/.repo/" "${S}/.repo"
      chown portage:portage -R "${S}"
   fi
}

src_unpack() {
   return
}

src_prepare(){

   repo init -u https://github.com/androvm/platform_manifest.git -b androVM-4.1.1_r6.1 || die
   repo sync || die
   make update-api

}

src_configure(){
#    source build/envsetsup.sh || die
   return
}

src_compile(){
   return
#    make ARCH=x86 -j12 || die
#    make ARCH=x86 android_disk_vdi -j12 || die
}

src_install(){

#    dodir "${INSTALL_DIR}"
#    cp -rpP "${S}/" "${D}${INSTALL_DIR}/" || die
#    
   exeinto "${INSTALL_DIR}"  
   doexe "${FILESDIR}/mysetup.sh"
   doexe "${FILESDIR}/mybuild.sh"
   doexe "${FILESDIR}/myinstall.sh"
#    echo "rsync -arvu ${S}/ ${D}"
#    rsync -arvu "${S}/" "${D}" 
}

pkg_preinst(){

    enewgroup androvm
    enewuser androvm -1 -1 -1 androvm
}

pkg_postinst(){
   rsync -arvu "${S}/" "${INSTALL_DIR}"
   
   
   chown androvm:androvm -R "${INSTALL_DIR}"
   chmod g+w -R "${INSTALL_DIR}"
   
   elog "AndroVM is now fetched and installed to ${INSTALL_DIR}"
   elog ""
   elog "Next steps:"
   elog ">> cd ${INSTALL_DIR}"
   elog ">> ./mysetup.sh"
   elog ">> ./mybuild.sh"
   elog ""
   ewarn "Make sure you are androvm group to avoid permission issues:"
   ewarn ">> gpasswd -a <user> androvm"
   
}
# 
# CODEXL_DIR="opt/${P}"
# # pkg_setup() {
# # # 	enewgroup android
# # }
# # 
# pkg_preinst(){
#     dodir "${CODEXL_DIR}"
#     cp -rpP ${S}/* "${D}${CODEXL_DIR}" || die
#     
#     mkdir ${FILESDIR}
#     echo "PATH=\"/${CODEXL_DIR}/bin\"" > "${FILESDIR}/99${PN}"
#     doenvd "${FILESDIR}/99${PN}"
# #     mv AMD_CodeXL_Linux_x86_64_${PV}/Output_x86_64/release amdapp-codecl
# # 	rm -rf tools/lib/x86*
# }
# 
# pkg_postinst(){
#     env-update
# }
# 
# # src_configure() {
# #     econf --with-posix-regex
# # }
# 
# src_install(){
# 	declare CODEXL_DIR="/opt/${PN}"
# 	
# 	mv "${S}" "${D}"${MOZILLA_FIVE_HOME} || die
# 
# }

# src_install(){
# # 	dodoc tools/NOTICE.txt "SDK Readme.txt" || die
# # 	rm -f tools/NOTICE.txt "SDK Readme.txt"
# 
# 	dodir "${CODEXL_DIR}"
# 	cp -pPR AMD_CodeXL_Linux_x86_64_1.2.2484/Output_x86_64/release/* "${CODEXL_DIR}" || die "failed to install tools"
# 
# 	# Maybe this is needed for the tools directory too.
# # 	dodir "${ANDROID_SDK_DIR}"/{add-ons,build-tools,docs,extras,platforms,platform-tools,samples,sources,system-images,temp} || die "failed to dodir"
# 
# # 	fowners root:android "${ANDROID_SDK_DIR}"/{.,add-ons,build-tools,docs,extras,platforms,platform-tools,samples,sources,system-images,temp,tools} || die
# # 	fperms 0775 "${ANDROID_SDK_DIR}"/{.,add-ons,build-tools,docs,extras,platforms,platform-tools,samples,sources,system-images,temp,tools} || die
# # 
# # 	echo "PATH=\"${EPREFIX}${ANDROID_SDK_DIR}/tools:${EPREFIX}${ANDROID_SDK_DIR}/platform-tools\"" > "${T}/80${PN}" || die
# # 
# # 	SWT_PATH=
# # 	SWT_VERSIONS="3.7 3.6"
# # 	for version in $SWT_VERSIONS; do
# # 		# redirecting stderr to /dev/null
# # 		# not sure if this is best, but avoids misleading error messages
# # 		SWT_PATH="`dirname \`java-config -p swt-\$version 2>/dev/null\` 2>/dev/null`"
# # 		if [ $SWT_PATH ]; then
# # 			einfo "SWT_PATH=$SWT_PATH selecting version $version of SWT."
# # 			break
# # 		fi
# # 	done
# # 
# # 	echo "ANDROID_SWT=\"${SWT_PATH}\"" >> "${T}/80${PN}" || die
# # 
# # 	doenvd "${T}/80${PN}" || die
# # 
# # 	echo "SEARCH_DIRS_MASK=\"${EPREFIX}${ANDROID_SDK_DIR}\"" > "${T}/80${PN}" || die
# # 
# # 	insinto "/etc/revdep-rebuild" && doins "${T}/80${PN}" || die
# 
# }

# src_install() {
#     emake DESTDIR="${D}" install || die
# 
#     dodoc FAQ NEWS README
#     dohtml EXTENDING.html ctags.html
# 
# }
#src_install() {
	# You must *personally verify* that this trick doesn't install
	# anything outside of DESTDIR; do this by reading and
	# understanding the install part of the Makefiles.
	# This is the preferred way to install.
	#emake DESTDIR="${D}" install || die

	# When you hit a failure with emake, do not just use make. It is
	# better to fix the Makefiles to allow proper parallelization.
	# If you fail with that, use "emake -j1", it's still better than make.

	# For Makefiles that don't make proper use of DESTDIR, setting
	# prefix is often an alternative.  However if you do this, then
	# you also need to specify mandir and infodir, since they were
	# passed to ./configure as absolute paths (overriding the prefix
	# setting).
	#emake \
	#	prefix="${D}"/usr \
	#	mandir="${D}"/usr/share/man \
	#	infodir="${D}"/usr/share/info \
	#	libdir="${D}"/usr/$(get_libdir) \
	#	install || die
	# Again, verify the Makefiles!  We don't want anything falling
	# outside of ${D}.

	# The portage shortcut to the above command is simply:
	#
	#einstall || die
#}
