# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

DESCRIPTION="The Google repo tool"
HOMEPAGE="http://code.google.com/p/git-repo/"
SRC_URI="http://git-repo.googlecode.com/files/repo-1.19"

# MERGE_TYPE="binary"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND="
   >=dev-vcs/git-1.8.1.5
   net-misc/curl
"

RDEPEND="${DEPEND}"

# pkg_setup(){
#    mkdir tmp
#    cp ${P} tmp/
#    mv tmp ${P}
#    tar -cf ${P}
# }

src_unpack() {
    mkdir -p "${WORKDIR}/${P}"
    cp -pL "${DISTDIR}/${P}" "${S}/" || die
#     if [ "${A}" != "" ]; then
#         unpack ${A}
#     fi
}



src_install() {
   
#    mkdir -p "${WORKDIR}/${P}"
#    cd "${S}"
   
#    echo cp -pL "${DISTDIR}/${PN} ${S}/"
#    cp -pL "${DISTDIR}/${PN}" "${S}/" || die
   
    cp ${P} ${PN}
    exeinto "usr/bin"
    doexe ${PN}
#    mkdir -p "${D}usr/bin"
#    echo "cp -rpP ${S}/* ${D}usr/bin/" 
#    cp -rpP ${S}/* "${D}usr/bin/" || die
}


# pkg_preinst(){
# 
#    dodir "usr/bin"
#    
#    echo "cp -rpP ${S}/* ${D}usr/bin" 
#    cp -rpP ${S}/* "${D}usr/bin" || die
#     
# #    doexe ${P}
# 
# #    exeinto "/usr/bin"
# #    doexe ${P}
#    
# #    dodir "usr/bin"
# #    echo cp -pL "${DISTDIR}/${PN} ${D}usr/bin/"
# #    cp -pL "${DISTDIR}/${PN}" "${D}usr/bin/"
# #    
# #    dodir "usr/bin"
# #    
# #    echo cp -pL "${DISTDIR}/${PN} ${D}usr/bin/"
# #    cp -pL "${DISTDIR}/${PN}" "${D}usr/bin/"
# }