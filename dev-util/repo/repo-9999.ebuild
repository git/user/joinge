# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit git-2

DESCRIPTION="The Google repo tool"
HOMEPAGE="http://code.google.com/p/git-repo/"
EGIT_REPO_URI="https://gerrit.googlesource.com/git-repo"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND="
   >=dev-vcs/git-1.7.2
   net-misc/curl
"

RDEPEND="${DEPEND}"


src_install() {
   
    exeinto "usr/bin"
    doexe ${PN}
}
