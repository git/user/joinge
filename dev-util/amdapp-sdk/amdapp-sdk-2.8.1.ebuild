

EAPI=5

DESCRIPTION="The AMD OpenCL SDK"
HOMEPAGE="http://developer.amd.com/tools-and-sdks/heterogeneous-computing/amd-accelerated-parallel-processing-app-sdk/downloads/"
# SRC_URI="http://developer.amd.com/wordpress/media/files/AMD-APP-SDK-v2.8.1.0-lnx64.tgz"
# SRC_URI="http://developer.amd.com/wordpress/media/files/AMD-APP-SDK-v2.8.1.0-lnx64.tgz"

MERGE_TYPE="binary"
RESTRICT="fetch"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64"

DEPEND="
   >=x11-drivers/ati-drivers-13.6
"

RDEPEND="${DEPEND}"

S="${WORKDIR}/AMD_CodeXL_Linux_x86_64_${PV}"

pkg_nofetch() {
    einfo "Please download"
    einfo "  - AMD-APP-SDK-v2.8.1.0-lnx64.tgz"
    einfo "from ${HOMEPAGE} and place it in ${DISTDIR}"
}


pkg_preinst(){
    
    echo "bash ${S}/Install-AMD-APP.sh"
    bash "${S}/Install-AMD-APP.sh"
    
    # SDK looks for this file in /usr/lib6 for some reason...
    dosym "/usr/lib32/libatiocl32.so" "/usr/lib64/libatiocl32.so"
    dosym "/usr/lib32/libamdocl32.so" "/usr/lib64/libamdocl32.so"
}

pkg_postinst(){
    env-update
}
